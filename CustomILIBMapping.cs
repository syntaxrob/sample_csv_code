using CsvHelper;
using CsvHelper.Configuration;
using SmartS.MVC3.ED.Models.ILIBUpload;
using System.IO;
using System.Linq;

namespace SmartS.MVC3.ED.Helpers
{
    public class CustomILIBMapping : ClassMap<MapViewModel>
    {
        public CustomILIBMapping()
        {
            var importedHeaders = GetHeaders().ToArray();
            //Want to recieve the model to be able to map in the same way I have done with importedHeaders

            var id = importedHeaders[0];//"Id";
            var uniqueId = importedHeaders[1]; //"UniqueIdentifier";
            var investment = importedHeaders[2];//"Investment";
            var investmentObjective = importedHeaders[3];//"InvestmentObjective";
            var sector = importedHeaders[4];//"Sector";
            var riskRating = importedHeaders[5];//"RiskRating";
            var initialChargePercentage = importedHeaders[6];//"InitialChargePercentage";
            var annualManagementChargePercentage = importedHeaders[7];//"AnnualManagementChargePercentage";
            var totalExpensePercentage = importedHeaders[8];//"TotalExpensePercentage";
            var ongoingChargesPercentage = importedHeaders[9];//"OngoingChargesPercentage";
            var dfmPercentage = importedHeaders[10];//"DFMPercentage";
            var transitionChargePercentage = importedHeaders[11];//"TransitionChargePercentage";
            var incidentalCosts = importedHeaders[12];//"IncidentalCosts";
            var furtherInfo = importedHeaders[13];//"FurtherInfo";

            //Map(m => m.Id).Name(id);
            Map(m => m.UniqueId).Name(uniqueId);
            Map(m => m.Investment).Name(investment);
            Map(m => m.InvestmentObjective).Name(investmentObjective);
            Map(m => m.Sector).Name(sector);
            Map(m => m.RiskRating).Name(riskRating);
            Map(m => m.InitialChargePercentage).Name(initialChargePercentage);
            Map(m => m.AnnualManagementChargePercentage).Name(annualManagementChargePercentage);
            Map(m => m.TotalExpensePercentage).Name(totalExpensePercentage);
            Map(m => m.OngoingChargesPercentage).Name(ongoingChargesPercentage);
            Map(m => m.DFMCharge).Name(dfmPercentage);
            Map(m => m.TransitionChargePercentage).Name(transitionChargePercentage);
            Map(m => m.IncidentalCosts).Name(incidentalCosts);
            Map(m => m.FurtherInfo).Name(furtherInfo);
        }

        public static string[] GetHeaders()
        {
            //var path = "D:\\Education\\csv_importer\\WebImporter\\Uploads\\NewFile.csv";
            //var path = "E:\\Genovo\\D\\Education\\csv_importer\\WebImporter\\Uploads\\NewFile.csv";
            var path = "C:\\genovo_app\\SmartS.MVC3.ED\\TempUploads\\Genovo11.csv";

            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader))
            {
                csv.Read();
                csv.ReadHeader();
                var importedHeaderRow = csv.Context.HeaderRecord;

                return importedHeaderRow;
            }
        }
    }
}