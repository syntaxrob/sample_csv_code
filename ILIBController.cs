//Get method to upload file
public ActionResult Upload()
        {
            return View();
        }

//Post method to upload file to the server
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                var path = GetPath();
                file.SaveAs(path);

                return RedirectToAction("SetMappings");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

//Get method for user to map headers from CSV against App model from drop down
        public ActionResult SetMappings()
        {
            MapViewModel model = new MapViewModel();

            var path = GetPath();

            string[] importedHeaderRow = UploadFileAndGetHeaders(path);

            model.Headers = new SelectList(importedHeaderRow);

            return View(model);
        }

//Send the users selection to a confirmation page
        [HttpPost]
        public ActionResult SetMappings(MapViewModel model)
        {
            // do stuff here with the mappings
            //var path = GetPath();

            //string[] importedHeaderRow = UploadFileAndGetHeaders(path);

            return RedirectToAction("ShowMappings", model);
        }

//Get method to View and confirm mappings 
        public ActionResult ShowMappings(MapViewModel model)
        {
            return View(model);
        }

//Post method where user can save, remap or cancel. On save this will be saved to the entity model
        [HttpPost]
        public ActionResult ShowMappings(MapViewModel model, FormCollection form)
        {
            var buttonOption = form["btnSubmit"];

            switch (buttonOption)
            {
                case "save":
                    var path = GetPath();

                    using (var reader = new StreamReader(path))
                    using (var csv = new CsvReader(reader))
                    {
                        csv.Configuration.RegisterClassMap<CustomILIBMapping>();
                        //HERE!!!! This is where I want to pass my model through to, to be able to map what the user has selected
                        //to the static headers
                        
                        // var map = csv.Configuration.AutoMap(model.GetType());
                        // var records = csv.GetRecords<MapViewModel>();
                        // var t = map.MemberMaps.ToArray();
                        // var sector = model.Sector;
                        // var headerSector = t[3].ToString();
                        
                        var data = records.ToList();

                        foreach (var item in data)
                        {
                            StdLibraryItem myStdLibraryItem = new StdLibraryItem
                            {
                                //Write away to the entity here
                            };
                            db.SaveChanges();
                        }
                    }

                    
                    DeleteCSV();
                    return RedirectToAction("Index");

                case "remap":
                    return RedirectToAction("SetMappings", model);

                case "cancel":
                    return RedirectToAction("Index");

                default:
                    return RedirectToAction("Index");
            }
        }

        public string GetPath()
        {
            //Remove any spaces from the OrgName
            var StdOrg = myCurrentUser.MyCompanyName;
            var StdOrgTrimmed = StdOrg.Trim().Replace(" ", "_");
            var StdOrgUserId = myCurrentUser.MyOrgUserId;
            var fileName = StdOrgTrimmed + StdOrgUserId + ".csv";
            var path = Path.Combine(Server.MapPath("~/TempUploads"), fileName);

            return path;
        }

        private string[] UploadFileAndGetHeaders(string path)
        {
            string[] importedHeaderRow;

            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader))
            {
                //Get Imported Headers
                csv.Read();
                csv.ReadHeader();
                importedHeaderRow = csv.Context.HeaderRecord;
            }

            return importedHeaderRow;
        }

        private void DeleteCSV()
        {
            var existingFile = GetPath();

            if (System.IO.File.Exists(existingFile))
            {
                System.IO.File.Delete(existingFile);
            }
        }