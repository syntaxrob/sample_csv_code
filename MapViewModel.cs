using System.ComponentModel;
using System.Web.Mvc;

namespace ILIBUpload
{
    public class MapViewModel
    {
        [DisplayName("Unique ID")]
        public string UniqueId { get; set; }

        [DisplayName("Investment")]
        public string Investment { get; set; }

        [DisplayName("Investment Objective")]
        public string InvestmentObjective { get; set; }

        [DisplayName("Sector")]
        public string Sector { get; set; }

        [DisplayName("Risk Rating")]
        public string RiskRating { get; set; }

        [DisplayName("Initial Charge Percentage")]
        public string InitialChargePercentage { get; set; }

        [DisplayName("Annual Management Charge Percentage")]
        public string AnnualManagementChargePercentage { get; set; }

        [DisplayName("Total Expense Percentage")]
        public string TotalExpensePercentage { get; set; }

        [DisplayName("Ongoing Charges Percentage")]
        public string OngoingChargesPercentage { get; set; }

        [DisplayName("DFM Charge")]
        public string DFMCharge { get; set; }

        [DisplayName("Transition Charge Percentage")]
        public string TransitionChargePercentage { get; set; }

        [DisplayName("Incidental Costs")]
        public string IncidentalCosts { get; set; }

        [DisplayName("Further Info")]
        public string FurtherInfo { get; set; }


        [DisplayName("Unique ID")]
        public SelectList Headers { get; set; }
    }
}